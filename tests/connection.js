/*npm install selenium-webdriver
npm install asserts
installer la version pour notre web browser (télécharger le .exe, le mettre dans le path)

https://www.youtube.com/watch?v=X3pTXG9a1oQ <- joli tuto pour la suite <3
http://www.softpost.org/selenium-with-node-js/assertions-in-selenium-in-node-js/
doc: http://seleniumhq.github.io/selenium/docs/api/javascript/index.html


//var driver = new webdriver.Builder().forBrowser('chrome').build(); pour chrome
*/



var webdriver = require('selenium-webdriver');
var assert = require('assert');
var driver = new webdriver.Builder().forBrowser('firefox').build();
driver.get('http://localhost:1337');
driver.findElement(webdriver.By.id('connectLink')).click();
driver.findElement(webdriver.By.id('pseudo')).sendKeys('mail@mail.mail');
driver.findElement(webdriver.By.id('pswd')).sendKeys('Mail123456');
driver.findElement(webdriver.By.id('buttSignin')).click();
driver.wait(webdriver.until.elementLocated(webdriver.By.id('auth'), 1000));
driver.sleep(1000);
driver.findElement(webdriver.By.id('auth')).getText().then(
  function(hello) {
    //console.log("hello is " + hello);
    assert.equal("Bonjour ben"===hello,true,"Error lors de la connection.");
  }
);
/*promise.then(function(hello) {
  console.log("hello is " + hello);
  assert.equal("Bonjour ben"===hello,true,"Error lors de la connection.");
});*/
//assert.equal("Bonjour ben"===text,true,"Error lors de la connection.");
driver.quit();


/*setup gitlab
docker image: https://github.com/SeleniumHQ/docker-selenium/tree/master/NodeFirefox


*/
