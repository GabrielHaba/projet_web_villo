/**
 * Station.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	schema : true,
	autoPK : false,
	tableName : 'stations',
	autoCreatedAt:false,
	autoUpdatedAt :false,
	
	meta :{
		schemaName : 'web'
	},

	attributes: {

		num_station : {
			required:true,
			primaryKey : true,
			type : 'integer'
		},

		name : {
			type :'string'
		},

		latitude :{
			type:'float'
		},

		longitude:{
			type:'float'
		},
		
		address:{
			type:'string'
		}
	}
};

