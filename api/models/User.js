/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  	schema : true,
	autoPK : false,
	tableName : 'users',
	autoCreatedAt:false,
	autoUpdatedAt :false,
	
	meta :{
		schemaName : 'web'
	},

	attributes: {

		num_user : {
			primaryKey : true,
      		type : 'integer',
      		autoIncrement : true,
		},

		email : {
      		type :'email',
      		unique : true
		},

		password :{
			type:'string'
		},

		first_name:{
			type:'string'
		},
		
		last_name:{
			type:'string'
		}
	}
};

