/**
 * Notification.js
 *
 */

 module.exports = {
 	schema : true,
 	autoPK : false,
 	tableName : 'notifications',
 	autoCreatedAt:false,
 	autoUpdatedAt :false,

 	meta :{
 		schemaName : 'web'
 	},

 	attributes: {

 		num_notification : {
			autoIncrement : true,
 			primaryKey : true,
 			type : 'integer'
 		},

 		notif_day : {
 			type :'string'
 		},

 		notif_hour :{
 			type:'string'
 		},

 		notif_type:{
 			type:'string'
 		},

 		num_user:{
 			type:'integer'
 		},

        num_station :{
            type: 'integer'
        }
 	}
 };
