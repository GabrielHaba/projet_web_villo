/**
 * RealTimeData.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  schema : true,
  autoPK : false,
  tableName : 'real_time_data',
  autoCreatedAt:false,
  autoUpdatedAt :false,

  meta :{
      schemaName : 'web'
  },

  attributes: {

    id_real_time : {
      primaryKey : true,
      type : 'integer',
      autoIncrement : true,
    },
    
    num_station : {
      type :'integer'
    },

    status : {
      type:'string'
    },

    
    bike_stands:{
      type:'integer'
    },

    banking:{
      type:'boolean'
    },

    bonus:{
      type:'boolean'
    },

    available_bikes : {
      type:'integer'
    },

    available_bike_stands : {
      type:'integer'
    },
    
    date_data : {
      type:'datetime'
    }
    
  }
};

