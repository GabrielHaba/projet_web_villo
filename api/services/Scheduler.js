//import { error } from 'util';

/***********************************************************
 *
 * Scheduler is a module that allows us to send automatically
 * mail to our costumer depending on their preferences stored
 * in the database
 *
 ***********************************************************/
const schedule = require('node-schedule');
const _env=require("../../config/env/development");
const mail = require("./Mail");
const Q=require('q');

var _currentDay;

var _tabFinal;

var _checkPreferencesTask=null;
var _retrieveDayNotificationsTasks=null;

var _realTimeData;
var _i;
var _json;

var _dataArray;

module.exports={

    scheduleDay : function(){
        var rule = new schedule.RecurrenceRule();
        if(_retrieveDayNotificationsTasks==null){
            rule.dayOfWeek = [ new schedule.Range(0,6,1)];
            var date=new Date();
            console.log("date "+date);
            rule.hour = date.getHours();
            rule.minute = date.getMinutes();
            rule.second =date.getSeconds()+10;
        }
        else{
            rule.dayOfWeek = [ new schedule.Range(0,6,1)];
            rule.hour = 0;
            rule.minute = 0;
            rule.second =0;
        }
        //_currentDay=(new Date).getDay();
        retrieveDayNotifications(rule);
        //.then(tabPreferences =>{
        //   scheduleMail(tabPreferences);
        //})
       // .catch(error => {
         //   Logger.error(error);
         //   reject(error);
        //});

    },

    //Add dynamically the notification created by the user to the scheduler of the day
    addNotificationToday : function(user,notification){
        console.log("_currentDay "+_currentDay);
        console.log("add '"+notification.notif_day+"' '"+_env.scheduler.daysOfWeek[_currentDay]+"'");
        if(notification.notif_day==_env.scheduler.daysOfWeek[_currentDay]){
            console.log("addNotificationToday ok");
            fillJson(user,notification);
        }
    },

    //Remove dynamically the notification deleted by the user to the scheduler of the day
    removeNotificationToday : function(notification){
        console.log("notif remove : "+JSON.stringify(notification));
        console.log("remove '"+notification.notif_day+"' '"+_env.scheduler.daysOfWeek[_currentDay]+"'");
        if(notification.notif_day==_env.scheduler.daysOfWeek[_currentDay]){
            console.log("removeNotificationToday ok");
            for(var i=0;i<_tabFinal.length;i++){
                console.log(_tabFinal[i]);
                if(_tabFinal[i]!=undefined && _tabFinal[i].num_notification==notification.num_notification)
                    delete _tabFinal[i];
            }
        }
    }
}

function fillDicoStations(tabStations){
    return new Promise((resolve,reject)=>{
        //console.log("fillDico");
        _realTimeData= {};
        for(var i=0;i<tabStations.length;i++){
            _realTimeData[tabStations[i].number]=tabStations[i];
        }
        resolve();
    })
}

function getUserByNotification(notificationsDay) {
        //console.log("notificationsDay "+JSON.stringify(notificationsDay));
        var _operationsQueue=[];

        notificationsDay.forEach(function(notif) {
            //for each notification, we retrieve the user who create this notification
            //(in the database)
            _operationsQueue.push(DbAccess.getUserByNum(notif.num_user).then(userNotif => {
                fillJson(userNotif,notif);
            }));
        });
        //console.log(_operationsQueue.length);
        return Q.allSettled(_operationsQueue);
}

function retrieveDayNotifications(rule){
    //return new Promise(function(resolve,reject){
    console.log("retrieveDayNotifications");

    _retrieveDayNotificationsTasks = schedule.scheduleJob(rule, function(){
        console.log("_checkPreferencesTask : "+_checkPreferencesTask);
        if(_checkPreferencesTask !== null){
            _checkPreferencesTask.cancel();
            _checkPreferencesTask=null;
        }
        _currentDay=new Date().getDay();
        console.log('change day');
        console.log("day n° "+_currentDay+"  :  "+_env.scheduler.daysOfWeek[_currentDay]);
        _tabFinal=[];
        _i=0;

        //We retrieve all the notifications for today in the database
        RetrieveVilloData.retrieveIRTStatus()
        .then((stations) => fillDicoStations(stations))
        .then(() =>
            DbAccess.getNotificationsByDay(_env.scheduler.daysOfWeek[_currentDay])
        )
        .then((notificationsDay) => {getUserByNotification(notificationsDay)})
        .then(()=>{scheduleMail();})
        .catch(error => {
            Logger.error(error);
            reject(error);
        });
    });
    console.log("next invo retrieveDayNotif: "+_retrieveDayNotificationsTasks.nextInvocation());
    //})


}

//fill the json _tabFinal with all the necessary data to send a mail after
function fillJson(userNotif,notif){
        //console.log("heure : "+notif.notif_hour);
        _json={};
        _json["email"]=userNotif.email;
        _json["first_name"]=userNotif.first_name;
        _json["last_name"]=userNotif.last_name;
        _json["notif_hour"]=notif.notif_hour;
        _json["notif_type"]=notif.notif_type;
        _json["station_name"]=_realTimeData[notif.num_station+""].name;
        _json["notif_day"]=_env.scheduler.daysOfWeek[_currentDay];
        _json["num_notification"]=notif.num_notification;
        console.log(notif.num_notification);
        if(_json['notif_type']==='leave'){
            _json['leave'] = _realTimeData[notif.num_station+""].available_bike_stands;
            _json['take'] = null;
        }else{
            _json['take'] = _realTimeData[notif.num_station+""].available_bikes;
            _json['leave'] = null;
        }
        _tabFinal[_i++]=_json;
        console.log("fillJson"+JSON.stringify(_json));

}

//tabPreferences is an array of map contained the user mail
function scheduleMail(){
    var tabPreferences=_tabFinal;
    var rule = new schedule.RecurrenceRule();
    rule.minute = [ new schedule.Range(0,60,5)];

    _checkPreferencesTask = schedule.scheduleJob(rule, function(){

        //console.log("Dans schedule mail  : "+ JSON.stringify(tabPreferences));
        var currentDate  = new Date();
        var dataEmail = {};

        tabPreferences.forEach(function(prefDay){

            //If the preference hour corresponds to the current hour
            //then send a mail
            if(prefDay != undefined) {
                if(compareCurrentHourWith(prefDay["notif_hour"])==0){
                    console.log("envoi à "+prefDay.email);
                    mail.sendEmail(prefDay);
                }
            }
        });
        if(_checkPreferencesTask!==null)
            console.log("next invo checkPref: "+_checkPreferencesTask.nextInvocation());
    });

    //console.log("next invo checkPref: "+_checkPreferencesTask.nextInvocation());
}
/**
 * Compare the current hour with the param hour (HH:MM:SS)
 * @param {*} hour
 * @returns -1 if current hour is before the hour param
 *           0 if the hours are equals
 *           1 if current hour is after the hour param
 */
function compareCurrentHourWith(hour){
    var date = new Date();
    var h = date.getHours();
    var min = date.getMinutes();
    //Probleme la DB retourne le type time (13:33:00) or dans le model, ce type-là n'exite pas
    //il a le type datetime ou alors on utilise le type string -> besoin d'un wrapper
    //Je considère ici que l'heure est une string
    var arrayHour = hour.split(':');
    if(h<arrayHour[0]) return -1;

    if(h==arrayHour[0]){
        if(min<arrayHour[1]) return -1;
        if(min>arrayHour[1]) return 1;
        return 0;
    }

    return 1;

}

var _this = require('./Scheduler');
//mail.sendEmail({leave : null,take : 3,email:"skubiszewski.roman@gmail.com",station_name:"maison",first_name:"rom",last_name:"Sku"});
_this.scheduleDay();
