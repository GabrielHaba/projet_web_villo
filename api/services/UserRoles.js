/***********************************************************
* UserRole is a module that do the basics roles of a user and
* allows 3 operations :
*      - Subscribe a user
*      - Connect a user
*      - Disconnect a user
*
***********************************************************/



var _bcrypt = require('bcrypt');
var _env = require('../../config/env/development');


module.exports = {

    subscribe: function (user) {
        return new Promise(
            function (resolve, reject) {
                //Check if the data provided by the client are valid
                var messErr = checkNewUser(user);
                //Otherwise return a message error with the status code 400

                if (messErr !== null) {
                    Logger.error(messErr);
                    var json = {status:400, message:messErr};
                    reject(400, json);//400
                }
                DbAccess.getUserByEmail(user.email)
                .then(function(result){
                    if (result !== undefined){
                        var json = {status:400, message:"Cette adresse e-mail est déjà utilisée."};
                        reject(json);
                    }
                    else {
                        //Hash password
                        _bcrypt.hash(user.password, _env.bcryptData.saltRounds, function (err, hash) {
                            //Replace the plaintext password by the the hash
                            user.password = hash;
                            //Then store the user in the DB
                            insertIntoDb(user)
                            .then(result => {
                                resolve(result);
                            })
                            .catch(error => {
                                var json = {status:500, message:"Erreur du serveur"};
                                reject(json);
                            });
                        });
                    }
                })
                .catch(function(error){
                    var json = {status:500, message:"Erreur du serveur"};
                    reject(json);
                });
            });
        }

        ,

        connect: function (user) {
            var erreur;
            return new Promise(
                function (resolve, reject) {
                    DbAccess.getUserByEmail(user.email)
                    .then((result) => {
                        if (result == undefined) {
                            Logger.error("Pas de user avec l'email suivant", "mail", user.email);
                            erreur = "L'e-mail et le mot de passe ne correspondent pas.";
                            reject({status:400, message:erreur});
                        }
                        if (!comparePasswords(user.password, result.password)) {
                            erreur = "L'e-mail et le mot de passe ne correspondent pas.";
                            reject({status:400, message:erreur});
                        }
                        resolve(result);
                    })
                    .catch(error => {
                        reject({status:500, message:"Erreur du serveur"});
                    });
                });

            }
        }

        function insertIntoDb(user) {
            return new Promise(function (resolve, reject) {
                DbAccess.putUser(user)
                .then(result => {
                    Logger.info("user vient d'être insérer en db", "first name", result.first_name);
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
            });
        }

        function checkNewUser(user) {
            //TODO use regex
            var message = null;
            if (user.email === '') {
                message += "Veuilllez indiquer un email !";
            }
            if (!isEmail(user.email)) {
                message += "Veuillez indiquer un email conforme !";
            }
            if (!isGoodPwd(user.password)) {
                message += "Veuillez indiquer un mot de passe conforme !";
            }
            if (user.last_name === '') {
                message += "Veuillez indiquer votre nom !";
            }
            if (user.first_name === '') {
                message += "Veuillez indiquer votre prénom !";
            }
            return message;
        }
        function isGoodPwd(myVar) {
            var regExPassword = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", "g");
            return regExPassword.test(myVar);
        }
        function isEmail(myVar) {
            var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
            Logger.debug('reg: ' + regEmail.test(myVar));
            return regEmail.test(myVar);
        }

        function comparePasswords(passwordEntered, passwordEncrypted) {
            return _bcrypt.compareSync(passwordEntered, passwordEncrypted);
        }
