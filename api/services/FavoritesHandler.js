/*********************************************
* NotificationHandler receives the request
* to make to the database.
*
********************************************/

module.exports =  {

    // Checks if there a notifications for the station before removing it
    // if it is the case, returns a 409
    removeFavorite(user_id, station_id){
        return new Promise(function(resolve, reject){
            DbAccess.retrieveNotificationByStation(user_id, station_id)
                .then(function(result){
                    if (result.length !== 0){
                        // You can not delete the station
                        Logger.debug("undeleted noitfs", "undeleted noitfs", "undeleted noitfs !")
                        reject(409);
                    }
                    else {
                        DbAccess.removeFavorite(user_id,station_id)
                            .then(function(result) {
                                resolve(result);
                            })
                            .catch(function(error){
                                reject(error);
                            });
                    }
                })
                .catch(function(error){
                    reject(error);
                })
        });
    },


    removeFavoriteWithNotif(user_id, station_id){
        return new Promise(function(resolve, reject){
            DbAccess.deleteAllNotificationsForStation(user_id, station_id)
                .then(function(result){
                    for (var i = 0; i < result.length; i++){
                        Scheduler.removeNotificationToday(result[i]);
                    }
                    DbAccess.removeFavorite(user_id,station_id)
                        .then(function(result2) {
                            resolve(result2);
                        })
                        .catch(function(error){
                            reject(error);
                        });
                })
                .catch(function(error){
                    reject(error);
                })
        });
    }

}
