/***********************************************************
 * 
 * Mail is a module that allows us to send
 * mail to our costumer
 * 
 ***********************************************************/

const nodemailer = require('nodemailer');


let _transporter = nodemailer.createTransport({
    service : 'gmail',
    auth: {
        user: 'projetwebvillo@gmail.com',
        pass: 'projetwebvillo2017' 
    }
});

 module.exports = {

    sendEmail : function(data){
        var _disponibilite;
        if(data.leave!==null){
            _disponibilite = '<p>Nombre de places libres : '+data.leave+'</p>'
        }else{
            _disponibilite = '<p>Nombre de vélos disponibles : '+data.take+'</p>'
        }
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Villo" projetwebvillo@gmail.com', // sender address
            to: ''+data.email, // list of receivers
            subject: 'Station '+data.station_name+' Availability ', // Subject line
            //text: 'Mon message de test', // plain text body
            html: '<h2>Bonjour '+data.first_name+' '+data.last_name+',</h2>'+
            '<p>Voici les disponibilités de la station '+data.station_name+' : </p>'+ _disponibilite // html body
        };
        //A tester : si on ne précise pas de callback a la fonction sendMail elle renvoie une promesse
        //return _transporter.sendMail(mailOptions);
        return new Promise(function(resolve, reject){
              // send mail with defined transport object
            _transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    Logger.error(error.message);
                    reject(error);
                }
                else{
                    resolve(info);
                    console.log('Message sent: %s', info.messageId);
                }
                
   
            });

            
        });
        
      
    }
 }