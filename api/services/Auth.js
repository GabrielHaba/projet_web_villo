/*
 * Auth
 *
 *
 */

module.exports = {
    isAuthentified : function(req){
        var authUser = null;
        if(req.session.user !== undefined){
            Logger.debug("authentification via session");
            authUser = JSON.stringify(req.session.user);
        }
        else if(req.signedCookies.user !== undefined){
            Logger.debug("authentification via cookie");
            req.session.user = JSON.parse(req.signedCookies.user);
            authUser = req.signedCookies.user;
        }
        Logger.debug("User","authUser",authUser);
        return authUser;
    }
}
