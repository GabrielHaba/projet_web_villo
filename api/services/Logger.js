/***********************************************************
*Logger is a module that handels the logging system.
*   -save logs in a repository
*   -print them in the right format on the console
 ***********************************************************/

'use strict';
const winston = require('winston');
const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      timestamp: tsFormat,
      colorize: true,
      level: 'debug'
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `logs/-errors.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      json: false,
    })
  ]
});

/**
*This function build the string that is going to be written/print using the given arguments
**/
function buildMessage(type,args){

  if(args.length<1){
    var _str="LOG ERROR: no message!";
    return _str;
  }
  var _str=type+": "+args[0];
  if(((args.length-1)%2)!==0){
    _str+="\n\t\t-Logger misused: couple var:value not pair."
    return _str;
  }
  for(var i=1;i<args.length;i+=2){
    _str+="\n\t\t--"+args[i]+": "+args[i+1];
  }
  return _str;
}

module.exports ={

  /**
  *Usage: message, var1,val1, var2, val2, ..., varX, valX
  *Usage: message
  **/

  error(){
    logger.error(buildMessage("ERROR",arguments));
  },

  debug(){
    logger.debug(buildMessage("DEBUG",arguments));
  },

  info(){
    logger.info(buildMessage("INFO",arguments));
  }



}
