/*********************************************
* NotificationHanfler receives the request
* to make to the database.
*
********************************************/

const Q = require("q");


// This table allows us to convert the days from the request to
// the convention used in the database
const translate = {
    "l": "monday",
    "m": "tuesday",
    "M": "wednesday",
    "j": "thursday",
    "v": "friday",
    "s": "saturday",
    "d": "sunday"
}

module.exports =  {

    addNotification: function(json, user){
        return new Promise(function(resolve, reject){
            console.log(json);
            var jsonTable = json;
            var dayTable = jsonTable['days'];
            // addapt hour
            var hour = jsonTable['notif_hour'].split(':');
            //console.log(hour);
            var min = parseInt(hour[1]);
            // Minutes should be multiples of 5
            min  = min - (min % 5);
            var hourString = hour[0] + ":" + min + ":" + hour[2];
            // Will contain all db operations
            var _operationQueue = [];
            var _addedNotifs = [];
            for (var i = 0; i < dayTable.length; i++){
                var notification = {
                    notif_day: translate[dayTable[i]],
                    notif_hour: hourString,
                    notif_type: jsonTable['notif_type'],
                    num_user: user["num_user"],
                    num_station: jsonTable['num_station']
                };
                _operationQueue.push(addNotificationDb(notification, _addedNotifs,user));
            }
            // Waits for all operations to be done
            Q.allSettled(_operationQueue)
            .then(function(result){ // result contains the status of the operations contained in the queue
                resolve(_addedNotifs);
            }).catch(error => {
                reject(error);
            });
        });
    },

    removeNotification: function(num_notification,user_id){
        return new Promise(function(resolve, reject){
            DbAccess.removeNotification(num_notification,user_id)
            .then(result => {
                console.log("result"+ JSON.stringify(result));
                Scheduler.removeNotificationToday(result[0]);
                resolve(result[0]);
            })
            .catch(error => {
                reject(error);
            })
        });
    },

    retrieveUserNotification: function(user_id){
        return new Promise(function(resolve, reject){
            DbAccess.retrieveUserNotification(user_id)
            .then(result => {
                resolve(result);
            })
            .catch (error => {
                reject(error);
            })
        });
    }
}

// Private functions
function addNotificationDb(notification, addedNotifs, user){
    return new Promise(function(resolve, reject){
        DbAccess.addNotification(notification)
        .then(result => {
            Scheduler.addNotificationToday(user,result);
            addedNotifs.push(result);
            resolve(result);
        })
        .catch(error => {
            reject(error);
        });
    })
}
