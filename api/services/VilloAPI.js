/***********************************************************
 * VilloData is a module that connects to the Villo API and
 * allows two operations :
 *      - Retrieve the station data (location, name, ...)
 *      - Retrieve the in real time data of the stations
 *
 ***********************************************************/

// This allows us to retrieve data from the config/env/development file
var _apiData = require('../../config/env/development');

const _request = require('request');

// This method is the one that will be exported out of the module
// It gives in real time information about all the station.
// When the data has been retrieved, it executes a callback function
module.exports ={

    //Retrieve all the stations with the real time data
    retrieveAllStations : function () {
        return new Promise (function(resolve, reject){
            var _url = _apiData.villoData.inRealTimeStatus + _apiData.villoData.apiKey;
            _request.get(_url, function(error, response, body){
                if (error || response.statusCode !== 200){
                    reject(error);
                }
                else {
                    var data = JSON.parse(body);
                    resolve(data);
                }
            });
        });
    }
}
