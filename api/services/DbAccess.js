/***********************************************************
* DbAccess is a module that allows us to retrieve all
* the data from the database
*      - Retrieve all the stations
*      - Retrieve the data of the stations in real time from DB
*
* And also to write into the database
*      - Write the real time data that the API gave
***********************************************************/


module.exports ={

    /******************************************
    *
    * Station
    *
    ******************************************/
    getAllStations() {
        return new Promise (function(resolve, reject){
            Station.find().exec(function(err, stations){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(stations);
                }
            });
        });
    },


    getStationByNum(numStation) {
        return new Promise (function(resolve, reject){
            Station.find({ where: { num_station : numStation}}).exec(function(err,station){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(station[0])
                }
            });
        });

    },

    //Put a station in the database
    putStation(jsonStations){
        return new Promise (function(resolve, reject){
            Station.create({
                //Qd ca vient du json dynamique avec les real time data
                num_station : jsonStations.number,
                name : jsonStations.name,
                latitude : jsonStations.position.lat,
                longitude : jsonStations.position.lng,
                address : jsonStations.address
            }).exec(function(err, newStation){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(newStation);
                }
            });
        });

    },

    /******************************************
    *
    * Favorite
    *
    ******************************************/
    putFavorite(user, station){
        return new Promise (function(resolve, reject){
            FavoriteStations.create({
                num_station : station,
                num_user : user
            }).exec(function(err, newFavorite){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(newFavorite);
                }
            });
        });
    },


    removeFavorite(user, station){
        return new Promise (function(resolve, reject){
            FavoriteStations.destroy({ where: { num_user : user, num_station : station}}).exec(function(err,stations){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(stations);
                }
            });
        });
    },

    getFavorites(user){
        return new Promise (function(resolve, reject){
            FavoriteStations.find({ where: { num_user : user}}).exec(function(err,stations){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(stations);
                }
            });
        });
    },

    /******************************************
    *
    * RealTimeData
    *
    ******************************************/

    getAllRealTimeData(){
        return new Promise (function(resolve, reject){
            RealTimeData.find().exec(function(err,realTimes){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    resolve(realTimes);
                }
            });
        });

    },

    putRealTimeData(jsonData){
        return new Promise (function(resolve, reject){
            RealTimeData.create({
                num_station : jsonData.number,
                status : jsonData.status,
                available_bikes : jsonData.available_bikes,
                available_bike_stands : jsonData.available_bike_stands,
                date_data : new Date(jsonData.last_update * 1e3).toISOString(),
                bike_stands: jsonData.bike_stands,
                banking: jsonData.banking,
                bonus : jsonData.bonus
            }).exec(function(err,newRealTimeData){
                if (err){
                    Logger.error(err.message," RealTimeData\n",JSON.stringify(newRealTimeData));
                    reject(err);
                }
                resolve(newRealTimeData);
            });
        });
    },

    updateRealTimeData(jsonData){
        return new Promise (function(resolve, reject){
            RealTimeData.update({num_station:jsonData.number},
                {
                    status : jsonData.status,
                    available_bikes : jsonData.available_bikes,
                    available_bike_stands : jsonData.available_bike_stands,
                    date_data : new Date(jsonData.last_update * 1e3).toISOString(),
                    bike_stands: jsonData.bike_stands,
                    banking: jsonData.banking,
                    bonus : jsonData.bonus
                }
            ).exec(function(err,newRealTimeData){
                if (err){
                    Logger.error(err.message,"Note","erreur lors de l'insertion");
                    reject(err);
                }
                resolve(newRealTimeData);


            });
        });
    },


    /******************************************
    *
    * User
    *
    ******************************************/


    getUserByEmail(emailSearched) {
        return new Promise(function (resolve, reject) {
            User.find({ where: { email: emailSearched } })
            .exec(function (err, user) {
                if (err) {
                    Logger.info(err.message, "Note", "erreur lors de l'insertion");
                    reject(err);
                }
                //We return the first (and unique here) user with this email
                else {
                    resolve(user[0]);
                }
            });
        });
    },

    putUser(jsonData) {
        return new Promise(function (resolve, reject) {
            User.create({
                email: jsonData.email,
                password: jsonData.password,
                first_name: jsonData.first_name,
                last_name: jsonData.last_name
            }).exec(function (err, newUser) {
                if (err) {
                    Logger.error(err.message, "Note", "erreur lors de l'insertion");
                    reject(err);
                }
                resolve(newUser);
            });
        });
    },

    getUserByNum(numSearched){
        return new Promise (function(resolve, reject){
            User.find({where : {num_user : numSearched}}).exec(function(err,user){
                if (err) {
                    Logger.error(err.message);
                    reject(err);
                }
                else {
                    //We return the first (and unique here) user with this num_user
                    resolve(user[0]);
                }
            });
        });
    },

    /******************************************
    *
    * Notifications
    *
    ******************************************/

    addNotification(json){
        return new Promise(function (resolve, reject){
            Notification.create({
                notif_day: json.notif_day,
                notif_hour: json.notif_hour,
                notif_type: json.notif_type,
                num_user: json.num_user,
                num_station: json.num_station
            }).exec(function(error, newNotif) {
                if (error) {
                    Logger.error(error.message);
                    reject(error);
                }
                else {
                    resolve(newNotif);
                }
            });
        })
    },

    removeNotification(num_notification,user_id){
        return new Promise(function (resolve, reject){
            Notification.destroy({
                where: {
                    num_notification : num_notification,
                    num_user : user_id
                }
            }).exec(function(error, removeNotif) {
                if (error) {
                    Logger.error(error.message);
                    reject(error);
                }
                else {
                    resolve(removeNotif);
                }
            });
        })
    },

    retrieveUserNotification(user){
        return new Promise(function (resolve, reject){
            Notification.find({where: {
                num_user: user
            }
        }).exec(function(error, notifications){
            if (error){
                Logger.error(error.message);
                reject(error);
            }
            else {
                resolve(notifications);
            }
        });
    })
},


getNotificationsByDay(day){
    return new Promise (function(resolve, reject){
        Notification.find({where: {
            notif_day: day
        }
    }).exec(function(error, notifications){
        if (error){
            Logger.error(error.message);
            reject(error);
        }
        else {
            resolve(notifications);
        }
    })
    });
},

retrieveNotificationByStation(user_id, station_id){
    return new Promise(function(resolve, reject){
        Notification.find({
            where: {
                num_user: user_id,
                num_station: station_id
            }
        }).exec(function(error, notifications){
            if (error) {
                Logger.error(error.message);
                reject(error);
            }
            else {
                resolve(notifications);
            }
        });
    });
},

deleteAllNotificationsForStation(user_id, station_id){
    return new Promise(function (resolve, reject){
        Notification.destroy({
            where: {
                num_station : station_id,
                num_user : user_id
            }
        }).exec(function(error, removeNotif) {
            if (error) {
                Logger.error(error.message);
                reject(error);
            }
            else {
                resolve(removeNotif);
            }
        });
    });
}
}
