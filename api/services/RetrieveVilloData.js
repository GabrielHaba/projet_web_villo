/***********************************************************
* RetrieveVilloData is a module that allows the us to retrieve
* the Villo data either form the database or the Villo API
*      - Retrieve the station data (location, name, ...) --> from DB
*      - Retrieve the in real time data of the stations --> from DB OR API
*
***********************************************************/

const _apiData = require('../../config/env/development');
const Q = require("q");
var _lastUpdate = undefined; // This will contain the last update timestamp
var _lastLiveData = undefined;
const _MS_PER_MINUTE = 60000;
const _MINS = 5;

// This will retrieve the IRT status of all the stations
// If the data has been updtated in the 5 last minutes
// it will retrieve data from the database
// otherwise, it will connect to the Villo API and write the
// new data in the database.
module.exports = {

    retrieveIRTStatus : function(){
        return new Promise(
            function(resolve, reject){
                var _fiveMinAgo = new Date(Date.now().valueOf() - (_MS_PER_MINUTE * _MINS));
                if (_lastUpdate !== undefined && _lastUpdate > _fiveMinAgo){
                    if (_lastLiveData === undefined){
                        Logger.error("Last live data not found");;
                        reject("Last live data not found");
                    }
                    else {
                        resolve( _lastLiveData);
                    }
                }
                else {
                    VilloAPI.retrieveAllStations()
                    .then(_realTimeDataArray => {
                        _lastLiveData = _realTimeDataArray;
                        _lastUpdate = Date.now();
                        resolve(_realTimeDataArray);
                        // Write the new Data in the DataBase
                        updateDataBase(_realTimeDataArray);
                    })
                    .catch (error => {
                        reject(error);
                    });
                }
            }
        );
    }
}

function updateDataBase(_rtData){
    // ---- DATABASE ACCESS ----
    var _setStationsInserted ;
    Promise.all([
        retrieveStations(),
        retrieveIRTData(),
    ])
    .then(([_stations, _irtStationsData]) => {
        var _operationQueue = [];
        writeStations(_rtData, _stations, _operationQueue);
        // Waits for all operations to end.
        Q.allSettled(_operationQueue).then(function(result){
            writeLiveData(_rtData, _irtStationsData);
        });
    })
    .catch(err => {
        throw new Error(err);
    });
}

function retrieveStations(){
    return new Promise(function(resolve, reject){
        var _set = new Set();
        DbAccess.getAllStations()
        .then(_stations => {
            for (var i = 0; i < _stations.length; i++) {
                _set.add(_stations[i].num_station);
            }
            resolve(_set);
        })
        .catch(error => {
            reject(error);
        });
    });
}

function retrieveIRTData(){
    return new Promise(function(resolve, reject){
        var _set = new Set();
        DbAccess.getAllRealTimeData()
        .then(_data => {
            for (var i = 0; i < _data.length; i++) {
                _set.add(_data[i].num_station);
            }
            resolve(_set);
        })
        .catch (error => {
            reject(error);
        });
    });
}


function writeStations(realTimeJson, stationsSet, operationQueue){
    for (var i = 0; i < realTimeJson.length; i++){
        var current = realTimeJson[i];
        if (!stationsSet.has(current.number)) {
            // Place operations in a queue to allow us to know when they are finished with the Q mechanism
            DbAccess.putStation(current)
            .then (_stationInserted => {
                operationQueue.push(_stationInserted);
            })
            .catch(error => {
                throw new Error(error);
            });
        }
    }
}

function writeLiveData(realTimeJson, liveDataSet){
    for (var i = 0; i < realTimeJson.length; i++) {
        var current = realTimeJson[i];
        if (!liveDataSet.has(current.number)) {
            DbAccess.putRealTimeData(current)
            .catch( error => {
                throw new Error(error);
            });
        }
        else {
            DbAccess.updateRealTimeData(current)
            .catch (error => {
                throw new Error(error);
            });
        }
    }
}
