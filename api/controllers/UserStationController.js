/**
 * UserStationController
 *
 * @description :: Server-side logic for managing stations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

    getFavorites: function (req,res) {
        var auth= Auth.isAuthentified(req);
        Logger.debug("authentification getFavorites","auth",auth);
        if(!auth)return res.send(401, "User not identified");
        var user_id = JSON.parse(auth)['num_user'];
        DbAccess.getFavorites(user_id)
        .then(result => {
            return res.ok(JSON.stringify(result));
        })
        .catch (error => {
            return res.negotiate(error);
        });
    },


    addFavorite : function(req, res){
        var auth= Auth.isAuthentified(req);
        if(!auth)return res.send(401, "User not identified");
        var user_id = JSON.parse(auth)['num_user'];
        Logger.debug("user_id","user_id",user_id);
        var station_id = JSON.parse(req.param('station_id'));
        DbAccess.putFavorite(user_id,station_id)
        .then(result => {
          Logger.debug("favorite inserted", "station_id", station_id,"user_id",user_id);
          res.ok(JSON.stringify(result));
        })
        .catch (error => {
            return res.negotiate(error);
        });


    },

    removeFavorite(req, res){
        var auth= Auth.isAuthentified(req);
        if(!auth)return res.send(401, "User not identified");
        var user_id = JSON.parse(auth)['num_user'];
        Logger.debug("user_id","user_id",user_id);
        var station_id = JSON.parse(req.param('station_id'));
        FavoritesHandler.removeFavorite(user_id,station_id)
        .then(result => {
          Logger.debug("favorite removed", "station_id", station_id,"user_id",user_id);
          res.ok(JSON.stringify(result));
        })
        .catch (error => {
            if (error === 409){
                return res.send(409, "Il reste des notifications pour cette favorite.");
            }
            return res.negotiate(error);
        });
    },

    removeFavoriteWithNotif(req, res){
        var auth = Auth.isAuthentified(req);
        if (!auth) {
            return res.send(401, "User not identified");
        }
        var user_id = JSON.parse(auth)['num_user'];
        Logger.debug("Delete fav with notif", "Delete fav with notif", user_id);
        var station_id = JSON.parse(req.param('station_id'));
        FavoritesHandler.removeFavoriteWithNotif(user_id,station_id)
        .then(result => {
          res.ok(JSON.stringify(result));
        })
        .catch (error => {
            return res.negotiate(error);
        });
    }

}
