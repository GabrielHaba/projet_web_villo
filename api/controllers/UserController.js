/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const _PROMISE = require("promise");
var _session = require('../../config/session');

module.exports = {

    signUp : function(req,res){
        var user = JSON.parse(req.param('user'));
        user.email = user.email.toLowerCase();
        UserRoles.subscribe(user)
            .then(result => {
                return res.ok();
            })
            .catch(function(error) {
                if (error.status === 400){
                    return res.send(400, error.message);
                }
                return res.negotiate(500);
            });
    },

    signIn : function(req,res){
        var user = JSON.parse(req.param('user'));
        user.email = user.email.toLowerCase();
        UserRoles.connect(user)
            .then((result)=>{
                result.password="";
                req.session.user=result;
                res.cookie('user', JSON.stringify(result), {signed:true,maxAge:_session.session.cookie.maxAge});
                return res.ok(result);
            })
            .catch(error=>{
                if (error.status === 400){
                    return res.send(400, error.message);
                }
                return res.negotiate(500);
            });
    },

    disco : function(req,res){
        try{
            req.session.destroy();
            res.clearCookie('user');
            res.ok();
        }catch(err){
            Logger.error("DiscoMessUp");
            res.negotiate(err);
        }
    },
    isAuthentified : function(req,res){
        var authUser = Auth.isAuthentified(req);
        if(authUser === null){
            return res.send(401,"Vous n'êtes pas authentifié ");
        }
        return res.ok(authUser);

    }

};
