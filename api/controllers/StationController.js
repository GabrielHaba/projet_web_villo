/**
 * StationController
 *
 * @description :: Server-side logic for managing stations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    //mettre des callback ou pas ?

    getAll: function (req,res) {
       try {
            //Services are globalized in Sails => we don't have to use require() to access
            DbAccess.getAllStations(function(stations){
                Logger.debug("getAllStations","stations.length",stations.length);
            });
        }catch(err){
          return res.negotiate(err);
        }

    },

    //Pourquoi controller  init -> RetrieveVilloData -> VilloApi -> controller putAll ?
    //Pourquoi pas plutot RetriveVilloData -> VilloApi puis RetriveVilloData -> putAll controller -> db
    //Initialize the stations in the database
    //call RetrieveVilloData.retrieveAllStations who calls _VilloAPI.retrieveAllStations who callback this.putAll
    initializeStations : function(){
        Logger.info("initializeStations");
        RetrieveVilloData.retrieveAllStations(function(mess){
            Logger.debug(mess);
        });
    }

};
