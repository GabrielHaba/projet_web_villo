/**
 * RealTimeDataController
 *
 * @description :: Server-side logic for managing Realtimedatas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const _PROMISE = require("promise");

module.exports = {


    getAllRealTimeData : function(req,res){
        RetrieveVilloData.retrieveIRTStatus()
            .then(result => {
                return res.ok(JSON.stringify(result));
            })
            .catch(error => {
                return res.negotiate(error)
            });
    }
};
