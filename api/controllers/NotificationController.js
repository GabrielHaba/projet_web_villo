/**
 * NotificationController
 *
 * @description :: Server-side logic for managing Notifications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


 module.exports = {
     addNotification: function (req,res) {
         var auth= Auth.isAuthentified(req);
         if (!auth){
             return res.send(401, "User not identified");
         }

         var notification = req.param('notification');
         if (notification === undefined){
             return res.send(400, "Bad request");
         }
         var user = JSON.parse(auth);
         NotificationHandler.addNotification(notification, user)
            .then(result => {
                res.ok(JSON.stringify(result))
            })
            .catch(error => {
                res.negotiate(error);
            });

     },

     removeNotification: function(req, res){
         var auth= Auth.isAuthentified(req);
         if (!auth){
             return res.send(401, "User not identified");
         }
         var num_notification = req.param('num_notification');
         console.log(num_notification);
         if (num_notification === undefined){
            return res.send(400, "Bad request");
        }
        var user_id = JSON.parse(auth)['num_user'];
         NotificationHandler.removeNotification(num_notification,user_id)
         .then(result => {
            res.ok(JSON.stringify(result))
        })
        .catch(error => {
            res.negotiate(error);
        });
     },

     retrieveUserNotification: function(req, res){
         var auth = Auth.isAuthentified(req);
         if (!auth){
             return res.send(401, "User not identified");
         }
         var user_id = JSON.parse(auth)['num_user'];
         // Send request to the database
         NotificationHandler.retrieveUserNotification(user_id)
            .then(result => {
                return res.ok(JSON.stringify(result));
            })
            .catch(error => {
                return res.negotiate(error);
            });
     }

 };
