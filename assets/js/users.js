$.ajax({
    url: '/myAccount',
    type: 'POST',
    success: function (data) {

        var user = (JSON.parse(data));
        var name = user.last_name+" "+user.first_name;
        RetrieveConnectedStations();
        $('#auth').text("Bonjour " + user.first_name);
        $('#auth').show();
        $('.disconnect').show();
        $('.subscribe').hide();
        $('.connect').hide();
        $('#notConnectedTable').hide();
        $('#connectedTable').show();
        initDeleteModal();
        initCancelDelete();

    },
    error: function (xhr, ajaxOptions, thrownError) {
        // DO NOTHING
    }
});

var creatUser = function () {
    function createAccount() {
        $('#butt-signup').click(function () {

            if (!$('#name').val() || !$('#firstName').val() || !$('#email').val() || !$('#password').val() || !$('#passwordConfirm').val()) {
                Materialize.toast('Tous les champs doivent être remplis!', 4000);
                return;
            }
            if(!isEmail($('#email').val())){
                Materialize.toast('Email invalide',4000);
                return;
            }
            //Must contains capital letter, numbers and lowercase letters and size 8
            var regExPassword = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", "g");
            if(regExPassword.test($('#password').val())== false){
                Materialize.toast('Attention le mot de passe doit contenir au moins 8 caractères avec des majuscules,nombres et minuscule',4000);
                return;
            }
            if ($('#password').val() !== $('#passwordConfirm').val()) {
                Materialize.toast('Les mots de passe ne correspondent pas.', 4000);
                return;
            }

            var user = {};

            user["last_name"] = $('#name').val();
            user["first_name"] = $('#firstName').val();
            user["email"] = $('#email').val().toLowerCase();
            user["password"] = $('#password').val();
            /*
            var user = {};

            user["last_name"] = "test";
            user["first_name"] = "test";
            user["email"] = "test@test.test";
            user["password"] = "test";
            */
            var json = JSON.stringify(user);
            $.ajax({
                url: '/signup',
                type: 'POST',
                data: {
                    action: 'signUp',
                    user: json
                },
                success: function () {
                    Materialize.toast('Inscription réussie.', 4000);
                    $("#signup").modal('close');
                    $("#connect").click();
                    $('#pseudo').val($("#email").val());
                    $('#pswd').val($("#password").val());
                    $('#signup').modal('close');
                    $('#buttSignin').click();
                    // empty form
                    $(':input').each(function(){$(this).val("")});
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.status === 400){
                        Materialize.toast(xhr.responseText, 4000);
                    }
                    else {
                        Materialize.toast('Erreur du serveur, veuillez réessayer plus tard.', 4000);
                    }
                }
            });
        });
    }
    function connect(){
        $('#buttSignin').click(function(){
            var user = {};
            user["email"] = $('#pseudo').val();
            user["password"] = $('#pswd').val();
            var json = JSON.stringify(user);

            $.ajax({
                url : "/signin",
                type : "POST",
                data : {
                    action : "signIn",
                    user : json
                },
                success : function(data){
                    Materialize.toast("Connexion réussie. ",4000);
                    $('#auth').text("Bonjour "  +data.first_name);
                    $('#auth').show();
                    $('.disconnect').show();
                    $('.subscribe').hide();
                    $('.connect').hide();
                    $('#notConnectedTable').hide();
                    $('#connectedTable').show();
                    $('#signin').modal('close');
                    RetrieveConnectedStations();
                    initDeleteModal();
                    initCancelDelete();
                    // Empty form
                    $(':input').each(function(){$(this).val("")});

                    //RetrieveFavoriteStation(); //to remove
                },
                error : function (err){
                    Materialize.toast(err.responseText,4000);
                }
            });

        });
    }
    function disconnect(){
       $('.disconnect').click(function(){
        $.ajax({
			url : '/disco',
			type : 'POST',
			data : {
				action : "disco"
			},
			success : function() {
                $('.subscribe').show();
                $('.connect').show();
                $('#auth').hide();
                $(".disconnect").hide();
                $('#notConnectedTable').show();
                $('#connectedTable').hide();
                hidePrefs();
                location.reload();
                Materialize.toast("Vous avez été déconnecté avec succès",4000);
			},
			error : function() {
                Materialize.toast("Le serveur ne répond pas, réessayer plus tard. ", 4000);
			}

		});
       });
    }
    // check if it's an email or nah
    function isEmail(myVar) {
        var regEmail = new RegExp('^[0-9a-z._-]+@{1}[0-9a-z.-]{2,}[.]{1}[a-z]{2,5}$', 'i');
        return regEmail.test(myVar);
    }

    var self = {
        createAccount : createAccount,
        connect : connect,
        disconnect : disconnect
	}
	return self;
}
