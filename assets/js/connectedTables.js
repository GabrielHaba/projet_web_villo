var all_stations = {};
var users_favorite = {};
var _jsonPref = {};

var currentStationtoDelete = undefined;

function RetrieveConnectedStations(){
    // Retrieve favorite stations
    $.ajax({
        url: '/retrieveIRTStations',
        type: 'POST',
        success: function(data){
            var JsonStations = JSON.parse(data);
            for (var i = 0; i < JsonStations.length; i++) {
                all_stations[JsonStations[i]['number']] = JsonStations[i];
            }
            RetrieveFavoriteStation();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            Materialize.toast(xhr.responseText, 4000);
        }
    });
}

function RetrieveFavoriteStation(){
    $.ajax({
        url: '/getFavorites',
        type: 'POST',
        success: function(response){
            var JsonArray = JSON.parse(response);
            for (var i = 0; i < JsonArray.length; i++){
                users_favorite[JsonArray[i]['num_station']] = JsonArray[i]['num_station'];
            }
            fillConnectedTables();
            loadPrefs();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            Materialize.toast(xhr.responseText, 4000);
        }
    });
}

function fillConnectedTables(){
    $('#favoriteTable tbody').empty();
    $('#allStationsTable tbody').empty();
	$.each(all_stations, function(index, value){
        // Favorite
        if (users_favorite[value['number']] !== undefined){
            $('#favoriteTable tbody').append('<tr class="valign-wrapper">'
                                    +'<td class="favoriteTd _stat_and_fav_td" id="sta'+ value['number'] + '" width="90%">'
                                        + value['name'].substr(5).split('/')[0] +'</td>'
                                    +'<td class="favoriteTd _stat_and_fav_td" width="10%"><i id="'+ value['number'] +'" class="star material-icons yellow-text text-darken-2">star</i></td>'
                                +'</tr>');
            $('#allStationsTable tbody').append('<tr class="valign-wrapper">'
                                    +'<td class="all_stations_td _stat_and_fav_td connectedTd" id="sta'+ value['number'] + '" width="90%">'
                                        + value['name'].substr(5).split('/')[0] +'</td>'
                                    +'<td class="all_stations_td _stat_and_fav_td connectedTd" width="10%"><i id="'+ value['number'] + '" class="star material-icons yellow-text text-darken-2">star</i></td>'
                                +'</tr>');
            addFavTdListener(value['number']);
            addAllStationListener(value['number']);
        }
        else {
            $('#allStationsTable tbody').append('<tr class="valign-wrapper">'
                                    +'<td class="all_stations_td _stat_and_fav_td connectedTd" id="sta'+ value['number'] + '" width="90%">'
                                        + value['name'].substr(5).split('/')[0] +'</td>'
                                    +'<td class="all_stations_td _stat_and_fav_td connectedTd" width="10%"><i id="'+ value['number'] + '" class="star material-icons">star_border</i></td>'
                                +'</tr>');

            addAllStationListener(value['number']);

        }
	});
    if ($('#favoriteTable tbody').children().length == 0) {
        $('#favoriteTable tbody').html("<tr><td>Vous n'avez aucune station dans vos favorites.</td></tr>");
    }


    addStarListener();


    //initializeTableSattionsListener();
    //$('#scrollable_tbody').scrollTop(0);
		//$('#scrollable_tbody').scrollTop($('#sta'+this.number).offset().top - $('#scrollable_tbody').offset().top);
    linkTableSearchConnected($('#autocomplete-favorites'), 'favoriteTd');
    linkTableSearchConnected($('#autocomplete-toutes'),'all_stations_td');
}

function addFavTdListener(id){
    $('#favoriteTable ._stat_and_fav_td[id="sta'+id+'"]').on('click', function () {
		if (currentTD !== undefined) {
			$(currentTD).parent().css('background-color', 'white');
		}
		var markerId = id;
		new google.maps.event.trigger(markers[markerId], 'click');
		currentTD = this;
		$(currentTD).parent().css('background-color', '#bbdefb');
	});
}

function addAllStationListener(id){
    $('#allStationsTable ._stat_and_fav_td[id="sta'+id+'"]').on('click', function () {
		if (currentTD !== undefined) {
			$(currentTD).parent().css('background-color', 'white');
		}
		var markerId = id;
		new google.maps.event.trigger(markers[markerId], 'click');
		currentTD = this;
		$(currentTD).parent().css('background-color', '#bbdefb');
	});
}

function addStarListener(){
    $('.star').on('click', function(){
        var station_id = $(this).attr("id");
        if (users_favorite[station_id] === undefined){
            Materialize.toast("Ajout de la station dans les favorites...", 4000);
            addStationToFavorite(station_id);
        }
        else {
            Materialize.toast("Retrait de la station dans les favorites...", 4000);
            removeStationFromFavorite(station_id);
        }
    });
}

function addStationToFavorite(station_id, star) {
    $.ajax({
        url: '/addFavorite',
        type: 'POST',
        data: {
            station_id: station_id
        },
        success: function(data){
            users_favorite[station_id] = all_stations[station_id];
            var value = all_stations[station_id];
            $(".star[id="+station_id+"]").css('color', '#fbc02d').text('star');
            $('#favoriteTable tbody').append('<tr class="valign-wrapper">'
                                    +'<td class="favoriteTd _stat_and_fav_td" id="sta'+ value['number'] + '" width="90%">'
                                        + value['name'].substr(5).split('/')[0] +'</td>'
                                    +'<td class="favoriteTd _stat_and_fav_td" width="10%"><i id="'+ value['number'] +'" class="star material-icons yellow-text text-darken-2">star</i></td>'
                                +'</tr>');
            Materialize.toast("La station a été ajoutée avec succès!", 4000);
            $('#linkPref').trigger('click');
            // Ajout d'un listener sur la td
            addFavTdListener(station_id);

            // Ajout d'un listener sur La nouvelle étoile...
            $('#favoriteTable .star[id="' + station_id +'"]').on('click', function(){
                var station_id = $(this).attr("id");
                if (users_favorite[station_id] === undefined){
                    Materialize.toast("Ajout de la station dans les favorites...", 4000);
                    addStationToFavorite(station_id);
                }
                else {
                    Materialize.toast("Retrait de la station dans les favorites...", 4000);
                    removeStationFromFavorite(station_id);
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError){
            Materialize.toast(thrownError, 4000);
        }
    });
}

function removeStationFromFavorite(station_id) {
    $.ajax({
        url: '/removeFavorite',
        type: 'POST',
        data: {
            station_id: station_id
        },
        success: function(data){
            delete users_favorite[station_id];
            $("#allStationsTable .star[id="+station_id+"]").text('star_border').removeClass( "yellow-text text-darken-2" )
            $("#favoriteTable .star[id=" + station_id + "]").parent().parent().remove();
            $("#allStationsTable .star[id="+station_id+"]").css('color', 'black');
            $('#linkPref').trigger('click');
            Materialize.toast("La station a été retirée avec succès!", 4000);
        },
        error: function(xhr, ajaxOptions, thrownError){
            // if we couldn't delete the station because there were notifications linked to it
            if (xhr.status === 409){
                $("#deleteFavModal").modal('open');
                currentStationtoDelete = station_id;
            }
            else {
                Materialize.toast("Erreur du serveur...", 4000);
            }
        }
    });
}

function initDeleteModal(){
    $('#deleteFavModal').on('click', function(){
        if (currentStationtoDelete !== undefined){
            $.ajax({
                url: '/removeFavoriteWithNotif',
                type:'PUT',
                data: {
                    station_id: currentStationtoDelete
                },
                success: function(result){
                    delete users_favorite[currentStationtoDelete];
                    $("#allStationsTable .star[id="+currentStationtoDelete+"]").text('star_border').removeClass( "yellow-text text-darken-2" );
                    $("#allStationsTable .star[id="+currentStationtoDelete+"]").css('color', 'black');
                    $('#linkPref').trigger('click');
                    $("#favoriteTable .star[id=" + currentStationtoDelete + "]").parent().parent().remove();
                    $('#allStationsTable td[id="sta'+currentStationtoDelete+'"]').trigger('click');
                    //Delete in the array of notifications the notifications deleted
                    delete _jsonPref[currentStationtoDelete];
                    currentStationtoDelete = undefined;
                    Materialize.toast("La station a été retirée avec succès!", 4000);
                },
                error: function(error){
                    Materialize.toast("Erreur du serveur...", 4000);
                }
            })
        }
    });
}

function initCancelDelete(){
    $('#cancelDelete').on('click', function(){
        currentStationtoDelete = undefined;
    });
}

function linkTableSearchConnected(input, tdClass) {
    input.on('input', function(){
		var inputed = input.val();
		var regex = new RegExp(inputed.toUpperCase().replace(/\s/g, '').removeAccents());
		$.each(stations, function(index, value){
			var _name=value['name'].substr(5).toUpperCase().replace(/\s/g, '').removeAccents();
			if(regex.test(_name)){
                $("." + tdClass + '[id="sta'+value["number"] +'"').parent().show();
			}
			else {
				$("." + tdClass + '[id="sta'+value["number"] +'"').parent().hide();
			}
		});
	});
}
