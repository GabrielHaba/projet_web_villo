// Generated from latin_map.pl Sun Jun 20 20:17:18 2010
var Accent = {};
Accent.accent_map = {
    "À": "A",
    "à": "a",
    "Â": "A",
    "â": "a",
    "Ä": "A",
    "ä": "a",
    "É": "E",
    "é": "e",
    "È": "E",
    "è": "e",
    "Ê": "E",
    "ê": "e",
    "Ë": "E",
    "ë": "e",
    "Ï": "I",
    "ï": "i",
    "Î": "I",
    "î": "i",
    "Ô": "O",
    "ô": "o",
    "Ö": "O",
    "ö": "o",
    "Û": "U",
    "û": "u",
    "Ù": "U",
    "ù": "u"
};

String.prototype.removeAccents = function() {
	return this.replace(/[^A-Za-z0-9\[\] ]/g, function(x) { return Accent.accent_map[x] || x; });
};
