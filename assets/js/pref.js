

var isEventRemoveDefined = false;
addEventOnStationTable($("#allStationsTable"));
addEventOnStationTable($("#favoriteTable"));

addEventOnPreferencesTab($("#linkPref"));

addEventOnSaveButton($("#pref_btn"));



function addEventOnStationTable(table){
  $(table).on("click","td",function(){
    if(isElemInTab(currentStation.number,users_favorite)){
      $("#pasPref").hide();
      $("#prefContent").show();
      loadContent(_jsonPref[currentStation.number]);

    }
    else{
      $("#prefContent").hide();
      $("#pasPref").show();

    }
  });
}

function addEventOnPreferencesTab(tab){
  $(tab).on('click',function(){
    if(isElemInTab(currentStation.number,users_favorite)){
      $("#pasPref").hide();
      $("#prefContent").show();
      loadContent(_jsonPref[currentStation.number]);
    }
    else{
      $("#prefContent").hide();
      $("#pasPref").show();
    }
  })
}

function addEventOnSaveButton(btn){
  $(btn).on('click',function(){
    saveNotififaction();
  });
}

function isElemInTab(elem,tab){
  for(var e in tab){
    if(e==elem)
      return true;
  }
  return false;
}

function loadPrefs(){
  $(".pref_window").css("visibility","visible");
  getPrefs();
}

function hidePrefs(){
  $(".pref_window").css("visibility","hidden");
}


function getPrefs(){
  $.ajax({
    url:'/retrieveUserNotification',
    type:'GET',
    success : function(res){
      _preferences=JSON.parse(res);
      initPrefJson(_preferences);
    },
    error : function(err){
      Materialize.toast("Le serveur ne répond pas, réessayer plus tard. ", 4000);
    }
  })
}


function initPrefJson(prefs){
  prefs.forEach(function(pref){
    if(_jsonPref[pref.num_station] == null){
      _jsonPref[pref.num_station]=new Array(pref);
    }
    else{
      _jsonPref[pref.num_station].push(pref);
    }
  })
}

function loadContent(prefSOfStation){
  loadTable(prefSOfStation);
  if(isEventRemoveDefined){
    return;
  }
  isEventRemoveDefined = true;
  $('#prefsTab').on('click','i',function(){
      var id=$(this).attr('id');
      id=id.substring(7);
      $.ajax({
        url:'/removeNotification',
        type: 'PUT',
        data : "num_notification="+id,
        success : function(data){
          Materialize.toast("La préférence a été supprimée !",4000);
          deleteNotification(data);

        },
        error : function(error){
            Materialize.toast("Le serveur ne répond pas, réessayer plus tard. ", 4000);
        }
      });
    });
}
function loadTable(prefSOfStation){
  $('#prefsTab td').text('');
  if(prefSOfStation==undefined){
    $("#prefsTab").html('');
    return;
  }
  var html = "<thead>";
  //html += "<tr><th>lundi</th></tr>";
  html += "<tr><th>jours</th><th>heure</th><th>action</th><th>remove</th></tr>";
  html += "</thead><tbody>";
  var i=0;
  prefSOfStation.forEach(function(pref) {
    if(pref!=undefined){
      html+= "<tr><td>"+pref.notif_day+"</td><td>"+pref.notif_hour+"</td><td>"+pref.notif_type+"</td><td><a class=\"btn-floating waves-effect waves-light waves-effect waves-light red\"><i id=\"delPref"+(pref.num_notification)+"\" class=\"delPref material-icons\">delete</i></a></td></tr>";
    }
  });
  
  html+= "</tbody>";
  $("#prefsTab").html(html);
}

function deleteNotification(data){

  var notification = JSON.parse(data);

  var notifStation = _jsonPref[notification.num_station];
  for(var i=0;i<notifStation.length;i++){

    if(notifStation[i]!==undefined && notifStation[i].num_notification == notification.num_notification){
      delete _jsonPref[notification.num_station][i];
    }
  }
  loadTable(_jsonPref[currentStation.number]);
}

function saveNotififaction(){
    var days = [];
    $('.days_checkbox:checked').each(function(){
        days.push($(this).attr("id"));
    });
  var hour=$("#timepicker_ampm_dark").val();
  if(hour==""){
    Materialize.toast("Précisez l'heure s'il vous plait !",4000);
    return;
  }
  if (days.length === 0){
      Materialize.toast("Vous devez choisir au moins un jour",4000);
      return;
  }
  var type = $("#switch_checkbox_pref").prop("checked");
  if(type===false){
      type="take";
  }else{
    type="leave";
  }


  //faire ajax ici :*
  $.ajax({
    url:'/addNotification',
    type: 'POST',
    data :
      {notification:
        {
          days : days,
          notif_hour : hour+':00',
          notif_type : type ,
          num_station : currentStation.number
        }
      },
    success : function(data){
      Materialize.toast("La préférence a été sauvée avec succès !",4000);
      clearDayCheckboxes();
      data=JSON.parse(data);
      for(var i=0;i<data.length;i++){
        if(_jsonPref[data[i].num_station] == null){
          _jsonPref[data[i].num_station]=new Array(data[i]);
        }
        else{
          _jsonPref[data[i].num_station].push(data[i]);
        }
      }
      loadTable(_jsonPref[currentStation.number]);
    },
    error : function(error){
        Materialize.toast("Le serveur ne répond pas, réessayer plus tard. ", 4000);
    }

  });
}

function clearDayCheckboxes(){
    $('.days_checkbox:checked').each(function(){
        $(this).prop("checked", false);
    });
    $("#timepicker_ampm_dark").val("");
}

var start_icon=false;
function switch_icons(){
  if(start_icon===true){
    $(".custom_switch_color_leave").css("color", "powderblue");
    $(".custom_switch_color_take").css("color", "#808080");
    $("#take_icon").hide();
    $("#leave_icon").show();
    start_icon=false;
  }else{
    $(".custom_switch_color_take").css("color", "#4885ed");
    $(".custom_switch_color_leave").css("color", "#808080");
    $("#take_icon").show();
    $("#leave_icon").hide();
    start_icon=true;
  }
}

var counter_for_label_containing_an_input=0;
$(".custom_switch").on('click',function(){
  counter_for_label_containing_an_input++;
  if(counter_for_label_containing_an_input%2===0){
    switch_icons();
  }
});

//Using: https://github.com/chingyawhao/materialize-clockpicker/
//Time Picker:
$('.timepicker').pickatime({
    default: 'now',
    twelvehour: false, // change to 12 hour AM/PM clock from 24 hour
    donetext: 'OK',
  autoclose: false,
  vibrate: true // vibrate the device when dragging clock hand
});
hidePrefs();
