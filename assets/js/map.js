var markers = {};
var stations = {};
var statName = {};
var statPosition = {};
var currentInfo = undefined;
var currentTD = undefined;
var panel, direction;
var currentStation = undefined;

var map = {
	myMap: undefined,
	newMap: function () {
		var mapProp = {
			center: new google.maps.LatLng(50.8503, 4.3517),
			zoom: 11,
		};
		myMap = new google.maps.Map(document.getElementById("googleMap"), mapProp);
		placeStations();
		$("#loader").css('display', 'none');
		$("#googleMap").css('display', 'inline-block');
	},

	addMarker: function (position, title, number) {
		var marker = new google.maps.Marker({
			position: position,
			title: title,
			number: number,
			map: myMap
		});
		return marker;
	}
}
function placeStations() {

	$.ajax({
		url: '/retrieveIRTStations',
		type: 'POST',
		success: function (stationsData) {
			var stationsJson = JSON.parse(stationsData);
			for (var i = 0; i < stationsJson.length; i++) {
				var latitude = stationsJson[i]['position']['lat'];
				var longitude = stationsJson[i]['position']['lng'];
				var title = stationsJson[i]['name'].substr(6);
				var number = stationsJson[i]['number'];
				var position = new google.maps.LatLng(latitude, longitude);
				var marker = map.addMarker(position, title, number);
				addInfoWindow(marker, stationsJson[i]);
				markers[number] = marker;
				stations[number] = stationsJson[i];
				statName[title] = null;
				statPosition[title] = number;
			}
			fillTable();
			$('#scrollable_tbody tr:first td').trigger('click');
			linkTableSearch($('#autocomplete-stations'));
			initialize();
			autocompleteIti();
			calcItineraire();
		},
		error: function (error) {
			Materialize.toast("Le serveur ne répond pas, veuillez essayer plus tard..", 4000);
		}
	});
}
function initialize() {
	direction = new google.maps.DirectionsRenderer();
	//direction.setMap(map);
	direction.setPanel(document.getElementById('panel'));
}

function addInfoWindow(marker, jsonStation) {
	var contentString = '<div id="content">' +
		'<div id="siteNotice">' +
		'</div>' +
		'<h5 id="firstHeading" class="firstHeading">' + jsonStation['name'].substr(5).split('/')[0] + '</h5>' +
		'<div id="bodyContent">' +
		'<p><h6>Adresse</h6>' + jsonStation['address'].split('/')[0] + '</p>' +
		'</div>' +
		'</div>' +
		'</div>';
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	marker.addListener('click', function () {
		if (currentTD !== undefined) {
			currentInfo.close();
			$(currentTD).parent().css('background-color', 'white');
		}
		currentTD = $('#sta' + this.number);
		$('#sta' + this.number).parent().css('background-color', '#bbdefb');


		$('.notConnectedTbody').scrollTop(0);
		$('.notConnectedTbody').scrollTop($('#sta' + this.number + '._stat_and_fav_td.notConnectedTd').offset().top - $('.notConnectedTbody').offset().top);

		$('.favTBody').scrollTop(0);
		var favTd = $('#sta' + this.number + '.favoriteTd._stat_and_fav_td').offset();
		if (favTd !== undefined) { $('.favTBody').scrollTop(favTd.top - $('.favTBody').offset().top); }

		$('.connectedTBody').scrollTop(0);
		var connTd = $('#sta' + this.number + '.all_stations_td._stat_and_fav_td.connectedTd').offset();
		if (connTd !== undefined) { $('.connectedTBody').scrollTop(connTd.top - $('.connectedTBody').offset().top); }

		infowindow.open(map, marker);
		currentInfo = infowindow;
		currentStation = stations[marker.number];
		displayMoreInfo(marker);
	});
}

function displayMoreInfo(marker) {
	var i = marker.number;
	$("#nameSta_pref").html(currentStation.name.substr(5).split('/')[0]);
	$('#nameSta').text(stations[i]['name'].substr(5).split('/')[0]);
	if (stations[i]['banking'] == true) {
		$('#bankingSta').html("<i style='font-size: 20px;' class='material-icons green-text'>check_circle</i>");
	} else {
		$('#bankingSta').html("<i style='font-size: 20px;' class='material-icons red-text'>block</i>");
	}
	if (stations[i]['bonus'] == 'true') {
		$('#bonusSta').html("<i style='font-size: 20px;'class='material-icons green-text'>check_circle</i>");
	} else {
		$('#bonusSta').html("<i style='font-size: 20px;' class='material-icons red-text'>block</i>");;
	}
	if (stations[i]['status'] == 'OPEN') {
		$('#statusSta').text("OUVERTE");
	} else {
		$('#statusSta').text("FERMEE")
	}
	$('#bikeStandSta').text(stations[i]['bike_stands']);
	$('#availableBikeStandSta').text(stations[i]['available_bike_stands']);
	$('#availableBikesSta').text(stations[i]['available_bikes']);
}
$(document).ready(function () {
	// the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
	$('.modal').modal();
	$('#return').on('click', function () {
		$(':input').each(function () { $(this).val(""); });
	});
	$('#retour2').on('click', function () {
		$(':input').each(function () { $(this).val(""); });
	});
	$('.button-collapse').sideNav({
		closeOnClick: true
	});
});

function fillTable() {
	$('#stationTable tbody').empty();
	$.each(stations, function (index, value) {
		$('#stationTable tbody').append('<tr><td id="sta' + value['number'] + '" class ="_stat_and_fav_td notConnectedTd">' + value['name'].substr(5).split('/')[0] + '</td></tr>')
	});
	initializeTableSattionsListener();
}



function initializeTableSattionsListener() {
	$('._stat_and_fav_td').on('click', function () {
		if ($(this).attr('id') === undefined){
			return ;
		}
		if (currentTD !== undefined) {
			$(currentTD).parent().css('background-color', 'white');
		}
		var markerId = $(this).attr('id').substr(3);
		new google.maps.event.trigger(markers[markerId], 'click');
		currentTD = this;
		$(currentTD).parent().css('background-color', '#bbdefb');
	});
}

function linkTableSearch(input) {
	input.on('input', function () {
		var inputed = input.val();
		var regex = new RegExp(inputed.toUpperCase().replace(/\s/g, '').removeAccents());
		$.each(stations, function (index, value) {
			var _name = value['name'].substr(5).toUpperCase().replace(/\s/g, '').removeAccents();
			if (regex.test(_name)) {
				$('#sta' + value["number"]).parent().css('display', 'block');
			}
			else {
				$('#sta' + value["number"]).parent().css('display', 'none');
			}
		});
		//initializeTableSattionsListener();
	});
}
//------------------------------------ PART CONCERNING THE PATH ------------------------------------------------------------

function calcItineraire() {
	$('#calculIti').click(function () {
		var depart = $('#autocomplete-input').val();
		var dest = $('#autocomplete-input2').val();
		var origin = statPosition[depart];
		var destination = statPosition[dest];
		if (depart && dest) {
			var request = {
				origin: stations[origin].position,
				destination: stations[destination].position,
				travelMode: google.maps.DirectionsTravelMode.BICYCLING
			};
			var directionsService = new google.maps.DirectionsService();
			directionsService.route(request, function (response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					direction.setDirections(response);
				}
				else {
					Materialize.toast("Nous avons un soucis, réessayez ", 4000);
				}
			});
		}
	});
}

function autocompleteIti() {
	$('input.autocomplete').autocomplete({
		data: statName,
		limit: 5,
	});
}
