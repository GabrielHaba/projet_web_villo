﻿DROP SCHEMA web CASCADE;

CREATE SCHEMA web;

CREATE TABLE web.users(
    num_user SERIAL CONSTRAINT num_user_users PRIMARY KEY,
    email varchar(100) NOT NULL,
    password varchar(100) NOT NULL,
    first_name varchar(100) NOT NULL,
    last_name varchar(100) NOT NULL
);

CREATE UNIQUE INDEX index_email_users ON web.users (email);

CREATE TABLE web.stations(
    num_station int CONSTRAINT num_station_stations PRIMARY KEY,
    name varchar(100) NULL,
    latitude float NULL,
    longitude float NULL,
    address varchar(300) NULL
);

CREATE TABLE web.real_time_data(
    id_real_time SERIAL CONSTRAINT id_real_time_real_time_data PRIMARY KEY,
    num_station int NULL CONSTRAINT num_station_real_time_data REFERENCES web.stations(num_station),
    status varchar(20) NULL CHECK(status IN ('OPEN','CLOSED')),
	bike_stands INT NULL,
    banking boolean NULL,
    bonus boolean NULL,
    available_bikes int NULL,
    available_bike_stands int NULL,
    date_data timestamp NULL
);

CREATE TABLE web.favorite_stations(
    num_user int NOT NULL CONSTRAINT num_user_favorite_stations REFERENCES web.users(num_user),
    num_station int NOT NULL CONSTRAINT num_station_favorite_stations REFERENCES web.stations(num_station),
    CONSTRAINT pk_favorite_stations PRIMARY KEY (num_user, num_station)
);

CREATE TABLE web.notifications(
    num_notification SERIAL CONSTRAINT num_notification_notifications PRIMARY KEY,
    notif_day varchar(50) NOT NULL,
    notif_hour varchar(10) NOT NULL,
    notif_type VARCHAR(20) NOT NULL CHECK(notif_type IN('take','leave')),
	num_user int NOT NULL,
    num_station int NOT NULL,
	CONSTRAINT fk_notifications FOREIGN KEY (num_user,num_station) REFERENCES web.favorite_stations(num_user,num_station)
);
